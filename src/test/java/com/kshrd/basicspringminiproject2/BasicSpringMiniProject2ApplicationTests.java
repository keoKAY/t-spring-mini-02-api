package com.kshrd.basicspringminiproject2;

import com.kshrd.basicspringminiproject2.repository.PostRepository;
import com.kshrd.basicspringminiproject2.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest(classes = BasicSpringMiniProject2Application.class)
@ExtendWith(SpringExtension.class)
class BasicSpringMiniProject2ApplicationTests {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    UserRepository repository;

    @Autowired
    PostRepository postRepository;

    @Test
    void contextLoads() {
     logger.info("Role in the database ->{}",repository.roleInDatabase());
    }

    @Test
    void findUserByUsername(){

        logger.info("User by username ->{}",repository.findUserByUsername("keo_kay"));
    }

    @Test
    void findPostByID(){
        logger.info("User by username ->{}",postRepository.findPostByID(2) );
    }


    @Test
    void testGettingPostComments(){
        logger.info("Getting post comment and replies : ->{}",postRepository.getComments(1));

    }   @Test
    void testGettingLogin(){
        logger.info("Getting post comment and replies : ->{}",repository.findUserByUsernameLogin("user_handsome"));

    }


}
