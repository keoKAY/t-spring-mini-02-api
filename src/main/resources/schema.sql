CREATE TABLE roles
(
    id   serial      NOT NULL
        CONSTRAINT roles_pk
            PRIMARY KEY,
    name varchar(20) NOT NULL
);

CREATE UNIQUE INDEX roles_id_uindex
    ON roles (id);

CREATE UNIQUE INDEX roles_name_uindex
    ON roles (name);

CREATE TABLE users
(
    id       serial            NOT NULL
        CONSTRAINT users_pk
            PRIMARY KEY,
    fullname varchar(30)       NOT NULL,
    username varchar(30)       NOT NULL,
    password varchar(100)      NOT NULL,
    role_id  integer DEFAULT 1 NOT NULL,
    img_url  varchar(256)
);

CREATE UNIQUE INDEX users_id_uindex
    ON users (id);

CREATE TABLE user_roles
(
    user_id integer NOT NULL
        CONSTRAINT user_roles_users_id_fk
            REFERENCES users
            ON UPDATE CASCADE ON DELETE CASCADE,
    role_id integer NOT NULL
        CONSTRAINT user_roles_roles_id_fk
            REFERENCES roles,
    CONSTRAINT user_roles_pk
        PRIMARY KEY (role_id, user_id)
);

CREATE TABLE posts
(
    id   serial    NOT NULL
        CONSTRAINT posts_pk
            PRIMARY KEY,
    caption     varchar(255),
    image       varchar(255),
    no_of_likes integer DEFAULT 0     NOT NULL,
    user_id     integer               NOT NULL
        CONSTRAINT posts_users_id_fk
            REFERENCES users,
    owner       boolean DEFAULT FALSE NOT NULL
);

CREATE UNIQUE INDEX posts_id_uindex
    ON posts (id);

CREATE TABLE comments
(
    id        serial       NOT NULL
        CONSTRAINT comments_pk
            PRIMARY KEY,
    content   varchar(516) NOT NULL,
    user_id   integer      NOT NULL
        CONSTRAINT comments_users_id_fk
            REFERENCES users,
    post_id   integer      NOT NULL
        CONSTRAINT comments_posts_id_fk
            REFERENCES posts,
    reply_id  integer,
    parent_id integer
);

CREATE UNIQUE INDEX comments_id_uindex
    ON comments (id);

CREATE TABLE replies
(
    id        serial NOT NULL,
    content   varchar(516),
    user_id   integer
        CONSTRAINT replies_users_id_fk
            REFERENCES users,
    post_id   integer
        CONSTRAINT replies_posts_id_fk
            REFERENCES posts,
    parent_id integer
        CONSTRAINT replies_comments_id_fk
            REFERENCES comments
);

CREATE TABLE user_likes
(
    user_id integer NOT NULL,
    post_id integer NOT NULL,
    CONSTRAINT user_likes_pk
        PRIMARY KEY (user_id, post_id)
);

CREATE TABLE user_dislikes
(
    user_id integer NOT NULL,
    post_id integer NOT NULL,
    CONSTRAINT user_dislikes_pk
        PRIMARY KEY (user_id, post_id)
);
