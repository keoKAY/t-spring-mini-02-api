package com.kshrd.basicspringminiproject2.security;

import com.kshrd.basicspringminiproject2.model.AuthUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


@Data
@NoArgsConstructor
@AllArgsConstructor

public class UserDetailsImp implements UserDetails {

    String username;
    String password;
// To ge the authorities
    public Collection<? extends GrantedAuthority>  authorities;

//    public UserDetailsImp(long id, String username, String password, List<GrantedAuthority> authorities) {
//        this.username = username;
//        this.password=password;
//        this.authorities = authorities;
//    }

    public static UserDetailsImp build(AuthUser user){

//        List<GrantedAuthority> authorities=user.getRoles()
//                .stream()
//                .map(e-> new SimpleGrantedAuthority(e.getRole()))
//                .collect(Collectors.toList());

        List<GrantedAuthority> authorities = user.getRoles()
                .stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());

        return  new UserDetailsImp(

                user.getUsername(),
                user.getPassword(),
                authorities
        );



    }




    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
