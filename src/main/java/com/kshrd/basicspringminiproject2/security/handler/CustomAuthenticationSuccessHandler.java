package com.kshrd.basicspringminiproject2.security.handler;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.PushBuilder;
import java.io.IOException;

@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
//    @Override
//    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authentication) throws IOException, ServletException {
//        AuthenticationSuccessHandler.super.onAuthenticationSuccess(request, response, chain, authentication);
//    }


    public enum UserRole{
        ADMIN,USER

    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        // if he has the admin role , let him go to the dashboard
        response.setStatus(HttpServletResponse.SC_OK);

        for (GrantedAuthority authority:authentication.getAuthorities()){
            if (UserRole.ADMIN.name().equals(authority.getAuthority())){
                response.sendRedirect("/dashboard");
                return;
            }
        }

        String attemptedURI =(String) request.getSession().getAttribute("ATTEMPTED_URL");
        if (StringUtils.hasText(attemptedURI)){
            response.sendRedirect(attemptedURI);
            return;
        }

        response.sendRedirect("/");
    }
}
