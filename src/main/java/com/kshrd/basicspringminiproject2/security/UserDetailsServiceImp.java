package com.kshrd.basicspringminiproject2.security;

import com.kshrd.basicspringminiproject2.model.User;
import com.kshrd.basicspringminiproject2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;


@Service
public class UserDetailsServiceImp implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User selectedUser = userRepository.findUserByUsername(username);

        if (selectedUser ==null) return null;
        else
        {
            UserDetailsImp userDetails = new UserDetailsImp();
                            userDetails.setUsername(selectedUser.getUsername());
                            userDetails.setPassword(selectedUser.getPassword());
                            return userDetails;
        }



    }
}
