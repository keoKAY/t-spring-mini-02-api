package com.kshrd.basicspringminiproject2.security.jwt;


import com.kshrd.basicspringminiproject2.security.UserDetailsImp;
import com.kshrd.basicspringminiproject2.security.UserDetailsServiceImp;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.config.annotation.rsocket.RSocketSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtUtils {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    private String jwtSecret ="ThisIsTheKey";
    private long jwtExpiration = 86400000;


    public String generateJwtToken(Authentication authentication){
        UserDetailsImp userPrinciple =(UserDetailsImp) authentication.getPrincipal();
        return  Jwts.builder()
                .setSubject(userPrinciple.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(new Date(new Date().getTime()+jwtExpiration))
                .signWith(SignatureAlgorithm.HS512,jwtSecret)
                .compact();

    }

    // Get username and provided token

    public String getUsernameFromJwtToken(String token){

        // should verify it first.
        if (validateToken(token)){
            return Jwts.parser()
                    .setSigningKey(jwtSecret)
                    .parseClaimsJws(token)
                    .getBody()
                    .getSubject();
        }else
            return  null;

    }

    // validate the token.
    public boolean validateToken(String authToken){

        try{
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;

        }catch (SignatureException e){  logger.error("Invalid JWT Signature : {}",e.getMessage());}
        catch (MalformedJwtException e){ logger.error("Invalid JWT token : {}",e.getMessage());}
        catch (ExpiredJwtException e){ logger.error("JWT token is expired :{}",e.getMessage());}
        catch (UnsupportedJwtException e){ logger.error("JWT token is not supported : {}",e.getMessage() );}
        catch (IllegalArgumentException e){ logger.error("JWT claims string is empty ;{}",e.getMessage());}
        catch (Exception e){logger.error("JWT Exception occurs : {}",e.getMessage());}

        return  false;
    }

}
