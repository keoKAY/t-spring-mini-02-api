package com.kshrd.basicspringminiproject2.security.jwt;

import com.kshrd.basicspringminiproject2.security.UserDetailsImp;
import com.kshrd.basicspringminiproject2.security.UserDetailsServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.html.HTML;
import java.io.IOException;

public class JwtTokenFilter extends OncePerRequestFilter {

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private UserDetailsServiceImp serviceImp;
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        try{
            String jwt= parseJwt(request);
            if (jwt !=null && jwtUtils.validateToken(jwt)){
                String username = jwtUtils.getUsernameFromJwtToken(jwt);
                UserDetailsImp userDetailsImp = (UserDetailsImp) serviceImp.loadUserByUsername(username);


                // Use this to authenticate
                UsernamePasswordAuthenticationToken authenticationToken=
                        new UsernamePasswordAuthenticationToken(userDetailsImp, null, userDetailsImp.getAuthorities());
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }

        }catch (Exception ex){ ex.printStackTrace();}
        filterChain.doFilter(request,response);
    }
    private String parseJwt (HttpServletRequest request){
                    String header = request.getHeader("Authorization");
                    String prefix= "Bearer";

                    if (StringUtils.hasText(header) && header.startsWith(prefix)){
                        return header.substring(prefix.length()); // only take the token, to verity
                    }
    return  null;
    }
}
