package com.kshrd.basicspringminiproject2.configuration;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class FileConfiguration implements WebMvcConfigurer {

    String serverPath="src/main/resources/images/";

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry){
      //WebMvcConfigurer.super.addResourceHandlers(registry);
      registry.addResourceHandler("/files/**").addResourceLocations("file:"+serverPath);
      registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
    }
}
