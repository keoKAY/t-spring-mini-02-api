package com.kshrd.basicspringminiproject2.configuration;

import com.kshrd.basicspringminiproject2.security.UserDetailsServiceImp;
import com.kshrd.basicspringminiproject2.security.handler.CustomAuthenticationEntryPoint;
import com.kshrd.basicspringminiproject2.security.handler.CustomAuthenticationSuccessHandler;
import com.kshrd.basicspringminiproject2.security.handler.CustomLogoutSuccess;
import com.kshrd.basicspringminiproject2.security.jwt.EntryPointJwt;
import com.kshrd.basicspringminiproject2.security.jwt.JwtTokenFilter;
import lombok.CustomLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.security.PrivateKey;
import java.util.Arrays;
import java.util.Collections;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {


    @Autowired
    public UserDetailsServiceImp userDetailsServiceImp;

    @Autowired
    private EntryPointJwt unauthorizedHandler;


    @Order(1)
    @Configuration
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    public class ApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {

        @Autowired
        PasswordEncoder passwordEncoder;
        @Bean
        public PasswordEncoder passwordEncoder(){ return new BCryptPasswordEncoder();
        }
        // Use this in order to verify the user in provided url
        @Bean
        public AuthenticationManager authenticationManager() throws Exception{
            return super.authenticationManager();
        }
        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth
                    .userDetailsService(userDetailsServiceImp)
                    .passwordEncoder(passwordEncoder);
        }

        @Override
        public void configure(WebSecurity web) throws Exception {
            super.configure(web);
            // firewall, privilege
            // used to ignore resources. file , static resource

            web.ignoring()
                    .antMatchers("/h2-console/**",
                            "/swagger-ui.html", "/resources/**", "/static/**", "/css/**", "/js/**", "/images/**",
                            "/resources/static/**", "/css/**", "/js/**", "/img/**", "/fonts/**",
                            "/images/**", "/scss/**", "/vendor/**", "/favicon.ico", "/auth/**", "/favicon.png",
                            "/v2/api-docs", "/configuration/ui", "/configuration/security",
                            "/webjars/**", "/swagger-resources/**", "/actuator", "/swagger-ui/**",
                            "/actuator/**", "/swagger-ui/index.html", "/swagger-ui/");
        }


        @Bean
        public JwtTokenFilter jwtTokenFilter() {
            return new JwtTokenFilter();
        }


        @Override
        protected void configure(HttpSecurity http) throws Exception {
            // super.configure(http);

            http
                    .cors().configurationSource(corsConfigurationSource()).and()
                    .csrf().disable()
                    .exceptionHandling()
                    .authenticationEntryPoint(unauthorizedHandler)
                    .and()
                    .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                    //.authorizeRequests()
                    //update from here
                    .antMatcher("/api/**")
                    .authorizeRequests()
                    .antMatchers("/api/**","api/v1/post/find-all","api/v1/post/filter","api/v1/post/{id}/view")
                    .permitAll()
                    .anyRequest().authenticated();
                /*    .antMatchers("/api/v1/auth/login"
                            ,"/api/v1/auth/signup",
                            "/api/v1/file/upload" ,
                            "/api/v1/posts" ,
                            "/api/v1/posts/{id}/view",
                            "/api/v1/posts/filter",
                            "/api/v1/posts/react")
                    .permitAll()
                    .anyRequest()
                    .authenticated();*/

            http.addFilterBefore(jwtTokenFilter(),UsernamePasswordAuthenticationFilter.class);


        }

        @Bean
        CorsConfigurationSource corsConfigurationSource(){
            CorsConfiguration configuration = new CorsConfiguration();
            configuration.setAllowedOrigins(Collections.singletonList("*"));
            configuration.setAllowedMethods(Arrays.asList("GET","POST","PUT","OPTIONS","DELETE","PATCH"));
            UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
            source.registerCorsConfiguration("/**",configuration.applyPermitDefaultValues());
            return  source;
        }
    }

    @Order(2)
    @Configuration
    public class FormLoginWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter{
        @Autowired
        private PasswordEncoder passwordEncoder;
        @Autowired
        private CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler;
        @Autowired
        CustomAuthenticationEntryPoint entryPointJwt;



        //

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//            super.configure(auth);
            auth.userDetailsService(userDetailsServiceImp)
                    .passwordEncoder(passwordEncoder);
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
//            super.configure(http);

            http
                    .csrf().disable()
                    .authorizeRequests()
                    .antMatchers("/login","/files/***").permitAll()
                    .antMatchers("/dashboard/**").hasAuthority("ADMIN")
                    .anyRequest()
                    .fullyAuthenticated()
                    .and()
                    .formLogin()
                    .loginProcessingUrl("/login")
                    .failureUrl("/login?error=true")
                    .loginPage("/login")
                    .successHandler(customAuthenticationSuccessHandler)// when login is success.
                    .usernameParameter("username")
                    .passwordParameter("password")
                    .permitAll()
                    .and()
                    .logout()
                    .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                    .logoutSuccessHandler(new CustomLogoutSuccess())
                    .deleteCookies("JSESSIONID")
                    .logoutSuccessUrl("/")
                    .and()
                    .exceptionHandling()
                    .authenticationEntryPoint(entryPointJwt);
        }

        @Override
        public void configure(WebSecurity web) throws Exception {
            super.configure(web);
//            web.ignoring()
//                    .antMatchers("/h2-console/**",
//                            "/swagger-ui.html", "/resources/**", "/static/**", "/css/**", "/js/**", "/images/**",
//                            "/resources/static/**", "/css/**", "/js/**", "/img/**", "/fonts/**",
//                            "/images/**", "/scss/**", "/vendor/**", "/favicon.ico", "/auth/**", "/favicon.png",
//                            "/v2/api-docs", "/configuration/ui", "/configuration/security",
//                            "/webjars/**", "/swagger-resources/**", "/actuator", "/swagger-ui/**",
//                            "/actuator/**", "/swagger-ui/index.html", "/swagger-ui/");
        }
    }


}
