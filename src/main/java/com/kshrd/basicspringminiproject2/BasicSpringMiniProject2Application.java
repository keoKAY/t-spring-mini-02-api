package com.kshrd.basicspringminiproject2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class BasicSpringMiniProject2Application {

    public static void main(String[] args) {
        SpringApplication.run(BasicSpringMiniProject2Application.class, args);
    }

}
