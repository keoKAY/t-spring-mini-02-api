package com.kshrd.basicspringminiproject2.repository;


import com.kshrd.basicspringminiproject2.model.Role;
import com.kshrd.basicspringminiproject2.model.User;
import com.kshrd.basicspringminiproject2.payload.dao.UserSignUpDao;
import com.kshrd.basicspringminiproject2.payload.userdto.UserLoginDto;
import com.kshrd.basicspringminiproject2.payload.userequest.AccountUpdateRequest;
import com.kshrd.basicspringminiproject2.payload.userequest.UserSignUpRequest;
import com.kshrd.basicspringminiproject2.repository.provider.UserProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository {

    @Select("select * from roles")
    List<Role> roleInDatabase();


    @Select("select * from users as ut where ut.username= #{username}")
    User findUserByUsername(String username);

    @Select("select * from users as ut where ut.username= #{username}")
    @Results(
            value = {
                    @Result(property = "id",column = "id"),
                    @Result(property = "roles",column = "id" , many = @Many(select = "findRolesByUsername")),
                    @Result(property = "username",column = "username")
            }
    )
    UserLoginDto findUserByUsernameLogin(String username);


    @Select("select name from roles as r inner join user_roles ur on r.id = ur.role_id where user_id=#{id}")
    List<String>findRolesByUsername(int id);

    @InsertProvider(type = UserProvider.class, method = "signUpUser")
//    @Options(useGeneratedKeys = true,keyColumn = "id", keyProperty = "id")
    Boolean signUpUser(@Param("user") UserSignUpRequest user);

    @InsertProvider(type = UserProvider.class , method = "assignRoleToUser")
    Boolean assignRoleToUser(@Param("role_id") int role_id,@Param("user_id")  int user_id);



    @Delete("delete from users where id=#{id}")
    Boolean removeUser( int id);

    @Update("update users set username=#{user.username},fullname=#{user.fullname},img_url=#{user.imageUrl} where id=#{id}  ")
//    @Result(property = "imageUrl",column = "img_url")
    Boolean updateUser(@Param("user") AccountUpdateRequest updateRequest, int id );



}
