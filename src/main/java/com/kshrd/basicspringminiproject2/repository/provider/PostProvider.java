package com.kshrd.basicspringminiproject2.repository.provider;

import com.kshrd.basicspringminiproject2.model.EReactionTypes;
import com.kshrd.basicspringminiproject2.payload.userdto.PostDto;
import com.kshrd.basicspringminiproject2.service.PostService;
import com.kshrd.basicspringminiproject2.service.UserService;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Types;

public class PostProvider {

    @Autowired
    PostService postService;
//
//    @Autowired
//    PostProvider(PostService postService)
//    {
//                this.postService = postService;
//    }

    public String updatePostOwnership(Boolean status, int user_id) {

        return new SQL() {{


            if (user_id == 0) {
                System.out.println(" value of id zero, so we update all the post ownership.");
                System.out.println("Status is : "+status);
                UPDATE("posts");
                SET("owner=#{status}");

            }else{
                System.out.println("just need to update ownership of user : "+user_id);
                System.out.println("Status is :"+status);

                UPDATE("posts");
                SET("owner=#{status}");
                WHERE("user_id=#{user_id}");
            }

        }}.toString();
    }


//    For update the post information and like or unlike

    public String updatePostInformation(EReactionTypes types, PostDto post, int id, int user_id,int numberOfLikes) {



        System.out.println("Provider number of like s; "+numberOfLikes);
        return new SQL() {{
            UPDATE("posts");


           if (types.equals(EReactionTypes.LIKE)){
               //like the table
               SET("no_of_likes=#{numberOfLikes}+1");
           }else if (types.equals(EReactionTypes.UNLIKE)){
               if (numberOfLikes<=0)
                   SET("no_of_likes=0");
               else
               SET("no_of_likes=#{numberOfLikes}-1");
           }
            else {
                SET("caption=#{post.caption}");
                SET("image=#{post.image}");
            }

            WHERE("id=#{id} and user_id=#{user_id}");
        }}.toString();
    }


//     Filtering the posts.
    //@Select("select * from posts  limit #{limit} offset #{offset}")
public String selectAndFilterPosts(int limit, int offset,int user_id,String caption){
        return new SQL(){{
            SELECT("*");
            FROM("posts");

            if (!caption.equals(" ") && user_id==0 ){
                WHERE("UPPER(caption)  like upper('%'|| #{caption} || '%')");
            }else
            if (user_id!=0 && caption.equals(" ")){
                WHERE("user_id=#{user_id}");
            } else if (!caption.equals(" ")){
                WHERE("UPPER(caption)  like upper('%'|| #{caption} || '%') and user_id =#{user_id}");
            }

            LIMIT("#{limit}");
            OFFSET("#{offset}");

        }}.toString();
}


    public  String deleteCommendAndReply(int comment_id,int post_id,int user_id,int parent_id,boolean isPostOwner){

        return new SQL(){{
//            System.out.println("======================");
//            System.out.println("Provider ParentID : "+parent_id);
//            System.out.println("isPostOwner: "+isPostOwner);
//            System.out.println("=====================");


            DELETE_FROM("comments");
            if (parent_id == 0) {
             //   System.out.println("We are workin on the comment.");
                // here we will delete the comments
                if (isPostOwner) {
                    WHERE("id=#{comment_id}");
                } else {// in case it is the owner of the comment
                    WHERE("id=#{comment_id} and post_id=#{post_id} and user_id=#{user_id}");
                }


            } else {
                // here we will delete the replies.
//                System.out.println("Now are working on the replies");
//                System.out.println("comment id : "+comment_id + " post id : "+post_id + " user_id : "+user_id + " parent_id : "+parent_id);


                if (isPostOwner) {
                    // he is the ownwer if his the post
                    WHERE("id=#{comment_id} and parent_id=#{parent_id}");
                } else {
                    WHERE("id=#{comment_id} and post_id=#{post_id} and user_id=#{user_id} and parent_id=#{parent_id}");
                }

            }
        }}.toString();
    }


    public  String  updateRepliesAndComments(int comment_id, int parent_id, String content,int user_id){

        return new SQL(){{

            UPDATE("comments");
            SET("content=#{content}");
            WHERE("id=#{comment_id} and user_id=#{user_id}");
            if (parent_id!=0)
                WHERE("parent_id=#{parent_id}");

        }}.toString();

    }
}