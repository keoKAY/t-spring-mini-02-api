package com.kshrd.basicspringminiproject2.repository.provider;

import com.kshrd.basicspringminiproject2.model.Role;
import com.kshrd.basicspringminiproject2.payload.dao.UserSignUpDao;
import com.kshrd.basicspringminiproject2.payload.userequest.UserSignUpRequest;
import lombok.Value;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

import java.util.List;

public class UserProvider {

    public String signUpUser(@Param("user") UserSignUpRequest user){
        return new SQL(){
            {
                INSERT_INTO("users");
                VALUES("username","#{user.username}");
                VALUES("fullname","#{user.fullname}");
                VALUES("password","#{user.password}");
                VALUES("img_url","#{user.image}");

            }

        }.toString();



    }



    public String assignRoleToUser(@Param("role_id") int role_id,@Param("user_id")  int user_id){

        return new SQL(){{
        INSERT_INTO("user_roles");
                    VALUES("user_id", "#{user_id}");
                    VALUES("role_id",   "#{role_id}" );

        }}.toString();
    }
}
