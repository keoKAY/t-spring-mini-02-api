package com.kshrd.basicspringminiproject2.repository;


import com.kshrd.basicspringminiproject2.model.Comment;
import com.kshrd.basicspringminiproject2.model.EReactionTypes;
import com.kshrd.basicspringminiproject2.model.Post;
import com.kshrd.basicspringminiproject2.payload.userdto.CommentDto;
import com.kshrd.basicspringminiproject2.payload.userdto.PostDto;
import com.kshrd.basicspringminiproject2.repository.provider.PostProvider;
import com.kshrd.basicspringminiproject2.repository.provider.UserProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository {

    @Select("insert into posts ( caption,image, no_of_likes,user_id,owner) values(#{post.caption},#{post.image},#{post.no_of_likes},#{post.user_id},#{post.owner}) returning id")
    Integer createPost(@Param("post") Post post);
    @Select("select * from posts where id=#{id}")
    @Results(
            value = {
            @Result(property = "id",column = "id"),
            @Result(property = "user_id",column = "user_id"),
            @Result(property = "username",column = "user_id",one = @One(select = "getAuthorName")),
            @Result(property = "comments",column = "id",many = @Many(select = "getComments")),
//            @Result(property = "categories",column = "id", many =  @Many(select = "getCategories"))

            })
    PostDto findPostByID(int id);


//    Find all posts

    @SelectProvider(type = PostProvider.class,method = "selectAndFilterPosts")
    @Results(
            value = {
                    @Result(property = "id" ,column = "id"),
                    @Result(property = "user_id",column = "user_id"),
                    @Result(property = "username",column = "user_id",one = @One(select = "getAuthorName")) ,
                    @Result(property = "comments",column = "id",many = @Many(select = "getComments")),
//                    @Result(property = "categories",column = "id",many = @Many(select = "getCategories"))
            }

    )
    List<PostDto> selectAllPosts(int limit, int offset,int user_id, String caption);

//    @Select("select category_type from categories as cate inner join post_cates pc on cate.id = pc.cate_id where post_id=#{post_id}")
//    List<String >getCategories(int post_id);

    @Select("select username from users where id=#{id}")
    String getAuthorName (@Param("id") int id);

    @Select("select count(id) from posts")
    Integer countTotalPost();
   // updatePostOwnership
   @UpdateProvider(type = PostProvider.class, method = "updatePostOwnership")
    Boolean updatePostOwnership(boolean status ,int user_id);

   //updatePostInformation(EReactionTypes types, PostDto post, int id)
    @UpdateProvider(type = PostProvider.class, method ="updatePostInformation" )
    Boolean updatePost(EReactionTypes types,PostDto post,int id, int user_id, int numberOfLikes);

    @Select("select no_of_likes from posts where id=#{id}")
    Integer getPostLikes (int id);

    @Select("select user_id from user_likes where post_id=#{post_id}")
    String userIdOfPost( int post_id);

    @Insert("insert into user_likes (user_id,post_id) values(#{user_id},#{post_id})")
    Boolean insertUserLikes(int user_id,int post_id);

    @Delete("delete from user_likes where post_id=#{post_id}")
    Boolean removeUserLikes(int post_id);


    @Select("select *\n" +
            "from comments\n" +
            "where post_id=#{post_id} and parent_id is null ;")
    @Results
            (
                    value = {
                            @Result(property = "id", column = "id"),
//                            @Result(property = "replies",column = "id",many = @Many(select = "getTheReplies")),
                            @Result(property = "content",column = "content"),//parent.
                            @Result(property = "user_id",column = "user_id"),//parent.
                            @Result(property = "replies",column = "id" ,many = @Many(select = "getTheReplies"))
                    }
            )
    List<CommentDto> getComments (@Param("post_id") int post_id);
//post_id=#{post_id} and
    @Select("select * from comments\n" +
            "where  parent_id is not null and parent_id=#{id}")
    @Results
            (
                    value = {
                            @Result(property = "id", column = "id"),
                            @Result(property = "content",column = "content"),
                            @Result(property = "user_id",column = "user_id"),

                    }
            )
    List<Comment> getTheReplies(@Param("id") int id);

@Insert("insert into comments (post_id,content,user_id) values (#{post_id},#{content},#{user_id})")
Boolean addComment(int post_id ,  String content,int user_id);

@Insert("insert into comments (post_id,parent_id,content,user_id) values (#{post_id},#{parent_id}, #{content},#{user_id})")
Boolean reply( int parent_id,int post_id,String content,int user_id); // parent_id


 @Select("select post_id from comments where id =#{comment_id}")
Integer findPostIdByCommentId(int comment_id);

// @Param("post_id") int post_id,  @Update("update posts set caption=#{post.caption},image=#{}")
//   Boolean updatePost(@Param("post") PostDto post);


    @Delete("delete from posts where id=#{id} and user_id=#{user_id}")
    Boolean deletePost(int id, int user_id);


   // @Delete("delete from comments where id=#{comment_id} and post_id=#{post_id} and user_id=#{user_id}")
    //(int comment_id,int post_id,int user_id,int parent_id,boolean isPostOwner)
   @DeleteProvider(type = PostProvider.class,method = "deleteCommendAndReply")
    Boolean deleteComment(int comment_id,int post_id, int user_id,int parent_id,boolean isPostOwner);

   @Select("select * from comments where id=#{id} and parent_id is null ")
   Comment findCommentByID(int id);


   // Update comments or replies

    @UpdateProvider(type = PostProvider.class, method = "updateRepliesAndComments")
    Boolean updateCommentReply(int parent_id,int comment_id,String content,int user_id);
}
