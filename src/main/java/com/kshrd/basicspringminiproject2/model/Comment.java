package com.kshrd.basicspringminiproject2.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
public class Comment {

 @JsonProperty("id")
 private int id;
 @JsonProperty("content")
 private String content;
 @JsonProperty("user_id")
 private int user_id;
 @JsonProperty("post_id")
 private int post_id;
 @JsonProperty("parent_id")
 private int parent_id;
//  @JsonProperty("parent_id")
// @JsonIgnoreProperties("parent_comment_id")

}
