package com.kshrd.basicspringminiproject2.model;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class AuthRole {

    private  int id;
    private ERole role;
}
