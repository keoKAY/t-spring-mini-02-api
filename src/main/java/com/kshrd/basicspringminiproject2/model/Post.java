package com.kshrd.basicspringminiproject2.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor

public class Post {
    private  int id;
    private String caption;
    private String image;
    private int no_of_likes;
    private int user_id;
    private Boolean owner;

}
