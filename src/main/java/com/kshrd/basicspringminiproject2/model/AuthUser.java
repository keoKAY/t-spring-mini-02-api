package com.kshrd.basicspringminiproject2.model;


import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.HashSet;
import java.util.Set;

@Data
@Accessors(chain = true)
@ToString
public class AuthUser {

private long id;
private String username;
private String password;
private Set<String> roles = new HashSet<>();

public AuthUser(String username,String password){
    this.username =  username;
    this.password = password;
}
}
