package com.kshrd.basicspringminiproject2.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class User {

    private int id;
    private String username;
    private String password;
    private String img_url ;
    private String fullname;



}
