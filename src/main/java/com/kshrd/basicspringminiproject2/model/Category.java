package com.kshrd.basicspringminiproject2.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@AllArgsConstructor
@Data
@ToString

public class Category {

    private int id ;
    private String category_type;


}
