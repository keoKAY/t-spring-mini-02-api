package com.kshrd.basicspringminiproject2.payload.userequest;


import com.kshrd.basicspringminiproject2.model.Role;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class UserSignUpRequest {

    private String fullname;
    private String username;
    private String password;
    private String image;

    List<String>  roles;
}
