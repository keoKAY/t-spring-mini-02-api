package com.kshrd.basicspringminiproject2.payload.userequest;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor

public class AccountUpdateRequest {

    String fullname;
    String imageUrl;
    String username;

}
