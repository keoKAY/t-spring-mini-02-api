package com.kshrd.basicspringminiproject2.payload.userequest;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PostUpdateRequest {
    private  String caption;
    private String image;
}
