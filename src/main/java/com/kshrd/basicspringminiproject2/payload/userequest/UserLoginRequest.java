package com.kshrd.basicspringminiproject2.payload.userequest;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor

public class UserLoginRequest {
    String username;
    String password;
}
