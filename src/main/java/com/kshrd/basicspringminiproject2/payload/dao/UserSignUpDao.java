package com.kshrd.basicspringminiproject2.payload.dao;

import com.kshrd.basicspringminiproject2.model.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserSignUpDao {
    private String fullname;
    private String username;
    private String password;
    private String image;

    List<Role> roles;
}
