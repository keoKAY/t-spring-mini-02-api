package com.kshrd.basicspringminiproject2.payload.userdto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString

public class UserLoginDto {

    String token ;
    int id;
    String username;
    List<String> roles;

}
