package com.kshrd.basicspringminiproject2.payload.userdto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kshrd.basicspringminiproject2.model.Comment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
public class CommentDto {
    private int id;
    private String content;
    private int user_id;
    private int post_id;
//    @JsonIgnoreProperties("parent_id")
    private int parent_id;

    List<Comment> replies;
}
