package com.kshrd.basicspringminiproject2.payload.userdto;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.kshrd.basicspringminiproject2.model.Comment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class PostDto {


        @JsonProperty("id")
        private  int id;
        private String caption;
        private  int no_of_likes;
        private String image;
        private  boolean owner;
        private int user_id;
        private String username;

//        List<String> categories;
        List<CommentDto> comments;
}
