package com.kshrd.basicspringminiproject2.payload.userdto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserSignUpDto {
        int id;
        String fullname;
        String username;
        String img_url;
        List<String> roles;

}
