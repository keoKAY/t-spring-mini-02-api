package com.kshrd.basicspringminiproject2.controller.restcontroller;


import com.kshrd.basicspringminiproject2.model.Role;
import com.kshrd.basicspringminiproject2.model.User;
import com.kshrd.basicspringminiproject2.payload.dao.UserSignUpDao;
import com.kshrd.basicspringminiproject2.payload.response.Response;
import com.kshrd.basicspringminiproject2.payload.userdto.UserLoginDto;
import com.kshrd.basicspringminiproject2.payload.userdto.UserSignUpDto;
import com.kshrd.basicspringminiproject2.payload.userequest.UserLoginRequest;
import com.kshrd.basicspringminiproject2.payload.userequest.UserSignUpRequest;
import com.kshrd.basicspringminiproject2.security.jwt.JwtUtils;
import com.kshrd.basicspringminiproject2.service.PostService;
import com.kshrd.basicspringminiproject2.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.print.DocFlavor;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/v1/auth")
public class AuthRestController {

    public UserService userService;
    public PostService postService;
    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    private AuthenticationManager authenticationManager;

    // learn to use the mapper

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public AuthRestController(UserService userService, PostService postService) {
        this.postService = postService;
        this.userService = userService;
    }

    @PostMapping("/signup")
    public Response<UserSignUpDto> signUp(@RequestBody UserSignUpRequest request) {
        try {

            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            request.setPassword(encoder.encode(request.getPassword()));

            boolean roleCondition = false;
            boolean nameCondition = false;

            try {
                // Boolean isNameExisted = false;
                User findUserByUsername = userService.findUserByUsername(request.getUsername());
                System.out.println("Find User BY Username : " + findUserByUsername);
                if (findUserByUsername == null) {
                    nameCondition = true;
                    System.out.println("User value is null hx ");
                } else {
                    nameCondition = false;
                    System.out.println("this is username is existed... ");
                    return Response.<UserSignUpDto>badRequest().setError("This username is already exist. Try different one.");
                }


            } catch (Exception ex) {
                logger.error("ERROR FETCHING DATA ->{}", ex.getMessage());
                return Response.<UserSignUpDto>exception().setError(ex.getMessage());
            }

            // Checking on the role condition
            List<Role> roleInDb = userService.roleInDatabase();
            // Changing request roles format to the standard format
            List<String> newFormatRoles = new ArrayList<>();
            if (request.getRoles().size() == 1 && request.getRoles().get(0).equals("string")) {
                newFormatRoles.add(("ROLE_USER"));
                request.setRoles(newFormatRoles);

            } else {

                newFormatRoles = request.getRoles();
                String prefix = "ROLE_";
                for (int i = 0; i < newFormatRoles.size(); i++) {
                    newFormatRoles.set(i, prefix + newFormatRoles.get(i).toUpperCase());

                }
                request.setRoles(newFormatRoles);
            }
            // Condition prevent index out of bound
            List<String> requestedRoles = request.getRoles();
            if (requestedRoles.size() > roleInDb.size()) {
                for (int i = requestedRoles.size() - roleInDb.size() - 1; i < requestedRoles.size(); i++) {
                    roleInDb.add(i, new Role(i, "------------X--------"));
                }
            }
            // values in the database
            String prefix = "ROLE_";
            for (int i = 0; i < roleInDb.size(); i++) {
                roleInDb.set(i, new Role(roleInDb.get(i).getId(), prefix + roleInDb.get(i).getName()));
            }

            List<String> roleDbString = new ArrayList<>();
            for (int i = 0; i < roleInDb.size(); i++) {
                roleDbString.add(roleInDb.get(i).getName());
            }
            // get the value that doesn't have in the database
            List<String> filteredRoles = requestedRoles.stream()
                    .filter(element -> !roleDbString.contains(element))
                    .collect(Collectors.toList());
            // Cause we dun want to duplicate values
            HashSet<String> roleUnify = new HashSet<>();
            filteredRoles.stream()
                    .map(roleUnify::add)
                    .collect(Collectors.toList());


            String rolesString = roleUnify.stream()
                    .map(String::valueOf)
                    .reduce("", (first, second) -> second + "  " + first);

            if (!rolesString.isEmpty() || !roleUnify.isEmpty()) {
                return Response.<UserSignUpDto>badRequest().setError("Roles: " + rolesString + " doesn't exist in the database");
            } else {
                roleCondition = true;
            }
            if (roleCondition && nameCondition) {

                HashSet<String> unifyRoles = new HashSet<>();
                request.getRoles().stream()
                        .map(unifyRoles::add)
                        .collect(Collectors.toList());

                List<String> unifyRoleList = new ArrayList<>(unifyRoles);
                Boolean signUp = userService.signUpUser(request);

                if (signUp) {
                    // assign roles
                    User registeredUser = userService.findUserByUsername(request.getUsername());
                    List<Role> roles = new ArrayList<>();
                    int count = 0;
                    for (int i = 0; i < roleInDb.size(); i++) {
                        for (int j = 0; j < unifyRoleList.size(); j++) {
                            if (roleInDb.get(i).getName().equals(unifyRoleList.get(j).trim())) {
                                roles.add(count, new Role(roleInDb.get(i).getId(), unifyRoleList.get(j)));
                                count++;
                            }
                        }


                    }


                    // insert the role of the user

                    System.out.println("====================: Roles :==============");
                    roles.stream().forEach(System.out::println);
//                    roles.stream().forEach(e-> userService.assignRoleToUser(registeredUser.getId(),e.getId()));

                    for (int i = 0; i < roles.size(); i++) {
                        boolean isAssign = userService.assignRoleToUser(roles.get(i).getId(), registeredUser.getId());
                        System.out.println("Is assign: " + isAssign);
                    }


                    UserSignUpDto response = new UserSignUpDto();
                    response.setId(registeredUser.getId());
                    response.setRoles(unifyRoleList);
                    response.setFullname(request.getFullname());
                    response.setImg_url(request.getImage());
                    response.setUsername(request.getUsername());

                    return Response.<UserSignUpDto>successCreate(). setPayload(response).setSuccess(true).setError("Register successfully.....");
                }


                return Response.<UserSignUpDto>badRequest().setError("SignUp Failed. Exception occurs......");

            } else {
                return Response.<UserSignUpDto>badRequest().setError("SignUp Failed. I have no idea what the hell is this.");
            }

        } catch (Exception ex) {

            System.out.println("Exception when getting user : " + ex.getMessage());
            return Response.<UserSignUpDto>exception().setError("Exception occurs. Signup process failed....");

        }


    }

    @PostMapping("/login")
    //  @AuthenticationPrincipal AuthenticationPrincipal authentication,
    public Response<UserLoginDto> login(@RequestBody UserLoginRequest request) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                request.getUsername()
                , request.getPassword()
        );
        UserLoginDto response = new UserLoginDto();
//        System.out.println(" Hello world .... ");
        try {
            Authentication authentication1 = authenticationManager.authenticate(usernamePasswordAuthenticationToken);


            //Generate Token
//            JwtUtils jwtUtils = new JwtUtils();
            String jwtToken = jwtUtils.generateJwtToken(authentication1);

            response = userService.findUserByUsernameLogin(request.getUsername());
            response.setToken(jwtToken);
//            if (loginUser != null) {
//                response.setUsername(loginUser.getUsername());
//                response.setId(loginUser.getId());
//            }
//            try {
//                postService.updatePostOwnership(true, loginUser.getId());
//            } catch (Exception ex) {
//                System.out.println("Error updating the ownership of the posts " + ex.getMessage());
//
//            }


            return Response.<UserLoginDto>ok().setPayload(response).setSuccess(true). setError("Login successfully....");
        } catch (Exception ex) {
            System.out.println("Exception occurs : " + ex.getMessage());
          /*  try {
                postService.updatePostOwnership(false, 0);
            } catch (Exception sqlException) {
                System.out.println("Exception when attempting to update the ownership of the post: " + sqlException.getMessage());

            }*/

            return Response.<UserLoginDto>notFound().setError("Wrong Credential...");
        }

    }


}
