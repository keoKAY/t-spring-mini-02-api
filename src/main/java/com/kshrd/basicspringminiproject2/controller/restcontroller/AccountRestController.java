package com.kshrd.basicspringminiproject2.controller.restcontroller;


import com.kshrd.basicspringminiproject2.model.User;
import com.kshrd.basicspringminiproject2.payload.response.Response;
import com.kshrd.basicspringminiproject2.payload.userdto.UserSignUpDto;
import com.kshrd.basicspringminiproject2.payload.userequest.AccountUpdateRequest;
import com.kshrd.basicspringminiproject2.security.UserDetailsImp;
import com.kshrd.basicspringminiproject2.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLOutput;

@RestController
@RequestMapping("api/v1/account")
public class AccountRestController {
    // Here I'll try to use the mapper.
    public UserService userService;

    public ModelMapper modelMapper= new ModelMapper();

    @Autowired
    AccountRestController(UserService userService){
        this.userService = userService;
    }

    @PatchMapping("{id}/update-profile")
    public Response<?> updateAccount (@AuthenticationPrincipal UserDetailsImp authentication, @PathVariable int id , @RequestBody AccountUpdateRequest request){
        try{

            String username = authentication.getUsername();
           if (username!=null){
               User findUser=userService.findUserByUsername(username);
               if (id!= findUser.getId()){
                  return   Response.<UserSignUpDto>notFound().setError("Provided Id doesn't matched...");
               }else{
                    // perform the update
                   // check for the username to see if it's unique or not
                   User unifyUsername = userService.findUserByUsername(request.getUsername());
                   if (unifyUsername!=null){
                       return Response.<String>badRequest().setError("This username is already taken. Try different one...");
                   }else{
                       userService.updateUser(request,id);

                       User user = userService.findUserByUsername(username);
                       System.out.println("user value : "+user);
                       UserSignUpDto response = modelMapper.map(user, UserSignUpDto.class);
                       return Response.<UserSignUpDto>updateSuccess().setPayload(response).setSuccess(true).setError("Successfully Updated the information..");
                   }
               }
           }else {
               return  Response.<String>notFound().setError("Provided authentication is not exist.");
           }
        }catch (Exception ex){

            System.out.println("Update User Exception: "+ ex.getMessage());
            return Response.<String >exception().setError("Failed to update the user information. Exception occurs. You may need to relogin to perform this actions.");
        }
        // they can update their username ?
         }



    @PatchMapping("{id}/close")
    public Response<?> disableAccount(@AuthenticationPrincipal UserDetailsImp authentication, @PathVariable int id){
        try{
            String username = authentication.getUsername();

            if (username!= null){
                User findUser = userService.findUserByUsername(username);

                if (id != findUser.getId()){
                    return Response.<String>exception().setError("Id is not exist in the database......");
                }else {
                    userService.removeUser(id);
                    return Response.<String>successDelete().setPayload("Successfully removed from the database....").setSuccess(true).setError("Successfully Remove From the Database.. ");
                }
            }else{
                    //
                return Response.<String>notFound().setError("User doesn't exist. User removal failed.....! ");

            }
            // if there is a user in the database

        }catch (Exception ex){
            System.out.println("Exception occurs "+ex.getMessage());
            // we don't do anything on the getAuthorizes.
            return Response.<String>notFound().setError("Authentication is required to perform this actions....! ");
        }


    }




}
