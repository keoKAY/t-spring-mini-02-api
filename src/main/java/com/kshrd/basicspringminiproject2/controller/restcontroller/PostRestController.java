package com.kshrd.basicspringminiproject2.controller.restcontroller;


import com.kshrd.basicspringminiproject2.model.Comment;
import com.kshrd.basicspringminiproject2.model.EReactionTypes;
import com.kshrd.basicspringminiproject2.model.Post;
import com.kshrd.basicspringminiproject2.model.User;
import com.kshrd.basicspringminiproject2.payload.response.Response;
import com.kshrd.basicspringminiproject2.payload.userdto.PostDto;
import com.kshrd.basicspringminiproject2.payload.userequest.PostUpdateRequest;
import com.kshrd.basicspringminiproject2.security.UserDetailsImp;
import com.kshrd.basicspringminiproject2.service.PostService;
import com.kshrd.basicspringminiproject2.service.UserService;
import com.kshrd.basicspringminiproject2.utility.Paging;
import org.apache.ibatis.annotations.Options;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import javax.print.DocFlavor;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/posts")
public class PostRestController {


    UserService userService;
    PostService postService;

    public PostRestController(UserService userService, PostService postService) {
        this.postService = postService;
        this.userService = userService;
    }

    ModelMapper modelMapper = new ModelMapper();

    @GetMapping("/find-all")
    public Response<List<PostDto>> findAllPost (@AuthenticationPrincipal UserDetailsImp authentication,  @RequestParam(required = false, defaultValue = "10") int limit, @RequestParam(required = false, defaultValue = "1") int page) {
        try {

            String currentUser = authentication.getUsername();
            User findUserByID = userService.findUserByUsername(currentUser);
            postService.updatePostOwnership(true,findUserByID.getId());


            Paging paging = new Paging();
            paging.setPage(page);
            paging.setLimit(limit);

            int offset = (page - 1) * limit;
            int totalCount = postService.countTotalPost();
            paging.setTotalCount(totalCount);

            List<PostDto> response = postService.findAllPosts(limit, offset,0," ");


            return Response.<List<PostDto>>ok().setPayload(response).setMetadata(paging).setError("Fetching the data successfully...");
        } catch (Exception exception) {
            Paging paging = new Paging();
            paging.setPage(page);
            paging.setLimit(limit);

            int offset = (page - 1) * limit;
            int totalCount = postService.countTotalPost();
            paging.setTotalCount(totalCount);

            try {
                postService.updatePostOwnership(false,0);
                List<PostDto> response = postService.findAllPosts(limit, offset,0," ");

                return Response.<List<PostDto>>ok().setPayload(response).setError("Fetching the data successfully......");
            }catch (Exception sqlException){
                System.out.println("Error finding the post : " + exception.getMessage());
                return Response.<List<PostDto>>exception().setError("SQL Exception occurs. Cannot retrieve the posts....");
            }




        }

    }

    @GetMapping("/filter")
    public Response<List<PostDto>> filterPost(@AuthenticationPrincipal UserDetailsImp authentication,  @RequestParam(defaultValue = " ")String caption,@RequestParam(defaultValue = "0")Integer userId,   @RequestParam(required = false, defaultValue = "10") int limit, @RequestParam(required = false, defaultValue = "1") int page){
        try{
            Paging paging = new Paging();
            paging.setPage(page);
            paging.setLimit(limit);

            int offset = (page - 1) * limit;
            int totalCount = postService.countTotalPost();
            paging.setTotalCount(totalCount);

            List<PostDto> response = postService.findAllPosts(limit, offset,userId,caption);
            return Response.<List<PostDto>>ok().setPayload(response).setMetadata(paging).setError("Fetch all the data successfully.......");

        }catch (Exception ex){
            System.out.println("Authentication Exception : "+ex.getMessage());

            return Response.<List<PostDto>>exception().setError("Exception is occurs......");
        }
    }


    @PostMapping("/create-post")
    public Response<PostDto> createPost(@AuthenticationPrincipal UserDetailsImp authentication, @RequestBody PostUpdateRequest request) {

        try {

            String username = authentication.getUsername();
            if (username != null) {

                try {
                    User findUser = userService.findUserByUsername(username);

                    Post map = modelMapper.map(request, Post.class);
                    map.setUser_id(findUser.getId());
                    map.setOwner(true);
                    Integer postID = postService.createPost(map);
                    // here we also have to add the categories





                    //  get the id ;
                    PostDto response = modelMapper.map(map, PostDto.class);
                    response.setUsername(findUser.getUsername());
                    response.setId(postID);
                    //System.out.println("Here is the response value : "+response);


                    return Response.<PostDto>successCreate().setPayload(response).setError("Create the post successfully...");

                } catch (Exception ex) {

                    System.out.println("SQL EXCEPTION : " + ex.getMessage());
                    return Response.<PostDto>exception().setError("Exception occurs when finding the user .....");
                }


            } else {
                return Response.<PostDto>notFound().setError("Wrong Credential.. Please enter your credential...");
            }


            //    postService.createPost(request);

        } catch (Exception ex) {
            System.out.println("Exception Error : " + ex.getMessage());
            return Response.<PostDto>exception().setError("Failed to perform this action. Authentication is needed.....");
        }

        //  return Response.<PostDto>ok().setError("There is nothing to see here ");

    }

    @GetMapping("{id}/view")
    public Response<PostDto> findPostById(@AuthenticationPrincipal UserDetailsImp authentication, @PathVariable int id) {

        try {
            String authUser = authentication.getUsername();

            try{
               User loginUser = userService.findUserByUsername(authUser);
                // update the ownership
                 postService.updatePostOwnership(true, loginUser.getId());
                PostDto response = postService.findPostByID(id);
               // System.out.println("Response : " + response);
                if (response==null)
                    return Response.<PostDto>badRequest().setError("The provided post id doesn't exist in the database ...");

                return Response.<PostDto>ok().setPayload(response).setError("Successfully fetch the post....");
            }catch (Exception ex){
                postService.updatePostOwnership(false,0);
                System.out.println("SQL EXCEPTION: " + ex.getMessage());
                return Response.<PostDto>exception().setError("Exception occurs.. Failed to review this specific post.");
            }

        } catch (Exception exception) {

            postService.updatePostOwnership(false,0);
            System.out.println("Exception Authorization : "+exception.getMessage());
            PostDto response = postService.findPostByID(id);
           // System.out.println("Response : " + response);
            if (response==null)
                return Response.<PostDto>badRequest().setError("The provided post id doesn't exist in the database ...");

            return Response.<PostDto>ok().setPayload(response).setError("Successfully fetch the post .....");

            //}
        }


    }

    @PatchMapping("{id}/update-post")
    public Response<PostDto> updatePost(@AuthenticationPrincipal UserDetailsImp authentication, @PathVariable int id, @RequestBody PostUpdateRequest request) {

        try {
            String username = authentication.getUsername();
            try {
                // the hell of code here...

                User loginUser = userService.findUserByUsername(username);

                PostDto formattedRequest = modelMapper.map(request, PostDto.class);
                boolean isUpdated = postService.updatePost(EReactionTypes.NORMAL, formattedRequest, id, loginUser.getId(), 0);

                if (isUpdated) {
                    PostDto findPostById = postService.findPostByID(id);
                    return Response.<PostDto>updateSuccess().setPayload(findPostById).setError("Update the post successfully.....");
                } else
                    return Response.<PostDto>badRequest().setError("Provided ID doesn't exist in our database ...");


            } catch (Exception sqlException) {
                System.out.println("Error when updating the post information." + sqlException.getMessage());
                return Response.<PostDto>exception().setError("SQL Exception occur when updating the post information");

            }

        } catch (Exception ex) {
            System.out.println("Authentication is needed in order to perform this action....");
            return Response.<PostDto>exception().setError("Authentication is needed in order to perform this action..");
        }

    }

    enum ReactionType {
        LIKE, UNLIKE
    }

    @PatchMapping("{id}/react")
    public Response<PostDto> reactPost(@AuthenticationPrincipal UserDetailsImp authentication, @PathVariable int id,@RequestParam(required = true,defaultValue = "LIKE") ReactionType types) {
        try {

            String loginUser = authentication.getUsername();// if authenticated then we gonna like the  post.

            User authUser = userService.findUserByUsername(loginUser);
            Integer numberOfLikes = postService.getPostLikes(id);

            System.out.println(numberOfLikes + " Number of like for this post.  ");


            PostDto request = new PostDto();
            EReactionTypes types1 = modelMapper.map(types, EReactionTypes.class);
            String user_id_like_table = postService.userIdOfPost(id);
            System.out.println(" USER ID : => " + user_id_like_table);

            if (user_id_like_table != null) // means that he used to like
            {
                //  if (types.equals(EReactionTypes.LIKE) )
                System.out.println("User already like the post......");

                postService.updatePost(EReactionTypes.UNLIKE, request, id, authUser.getId(), numberOfLikes);
                postService.removeUserLikes(id);
                //  else
                //  postService.updatePost(EReactionTypes.LIKE,request,id,authUser.getId(),numberOfLikes);
            } else {

                if (types.toString().equals(EReactionTypes.LIKE.toString())) {
                    System.out.println("User is null and we like the post..");
                    postService.insertUserLikes(authUser.getId(), id);
                    postService.updatePost(EReactionTypes.LIKE, request, id, authUser.getId(), numberOfLikes);
                } else {

                    if (numberOfLikes == 0) {

                        postService.insertUserLikes(authUser.getId(), id);
                        postService.updatePost(EReactionTypes.LIKE, request, id, authUser.getId(), numberOfLikes);
                    } else {
                        System.out.println("user is null and we unlike th post  ");
                        postService.removeUserLikes(id);
                        postService.updatePost(EReactionTypes.UNLIKE, request, id, authUser.getId(), numberOfLikes);

                    }

                }
            }

            //postService.updatePost(types1,request,id,authUser.getId(),numberOfLikes);
            PostDto response = postService.findPostByID(id);
            return Response.<PostDto>ok().setPayload(response).setError("You have reacted on the post successfully.");


        } catch (Exception ex) {
            System.out.println("Authentication Exception: " + ex.getMessage());
            return Response.<PostDto>exception().setError("Authenticated is needed to perform this action....");
        }


    }

    // Comment on post
    @PostMapping("{post_id}/comment")
    public Response<PostDto> comment(@AuthenticationPrincipal UserDetailsImp authentication, @PathVariable int post_id, String content) {
        try {

            String currentUser = authentication.getUsername();

            try {
                User authUser = userService.findUserByUsername(currentUser);
                PostDto findPostByID = postService.findPostByID(post_id);
                if (findPostByID==null)
                    return  Response.<PostDto>notFound().setError("The provided post_id doesn't not exist in the database..");

                Boolean isCommentAdded = postService.addComment(post_id,content,authUser.getId());

                if (isCommentAdded){
                    postService.updatePostOwnership(true,authUser.getId());
                    findPostByID = postService.findPostByID(post_id);
                    return Response.<PostDto>successCreate().setPayload(findPostByID).setError("Successfully comment on the post.....");
                }else {
                     postService.updatePostOwnership(false,0);
                    return Response.<PostDto>badRequest().setError("Provided Id doesn't exist in our database.");
                }

            }catch (Exception sqlException){
                System.out.println("Sql Exception: "+sqlException.getMessage());
            }


            return Response.<PostDto>exception().setError("Authentication is needed to perform this action.. try to login first.");

        }catch (Exception exception){
            System.out.println("Exception comment : "+exception.getMessage());
            return Response.<PostDto>exception().setError("Authentication is needed to perform this action.. try to login first.");
        }


    }
    @PostMapping("{comment_id}/reply")
    public Response<PostDto> reply(@AuthenticationPrincipal UserDetailsImp authentication, @PathVariable  int comment_id,String content){
        try{
            String currentUser = authentication.getUsername();
            User authUser = userService.findUserByUsername(currentUser);

            try {
                int postId =postService.findPostIdByCommentId(comment_id);

                // find comment by id
                Comment findCommentByID= postService.findCommentByID(comment_id);
                if (findCommentByID==null)
                    return Response.<PostDto>badRequest().setError("Provided comment_id doesn't exist in the database....Try different one.");

               Boolean isReplyAdded =  postService.reply(comment_id, postId, content,authUser.getId());
                if (isReplyAdded){
                 PostDto response = postService.findPostByID(postService.findPostIdByCommentId(comment_id));
//                    System.out.println("Comment ID : "+comment_id);
                    System.out.println("Here is the value of the post ID: "+ postService.findPostIdByCommentId(comment_id));

                    return Response.<PostDto>successCreate().setPayload(response);
                }else {
                    return Response.<PostDto>exception().setError("The provided ID doesn't exist in the database. Try different one.....");
                }

            }catch (Exception sqlException){
                System.out.println("Something is wrong with the reply script : "+ sqlException.getMessage());
                return Response.<PostDto>exception().setError("SQL Exception happends....... Try again...");
            }

        }catch (Exception exception){
            System.out.println("Authentication exception  : "+ exception.getMessage());
            return Response.<PostDto>exception().setError("Authentication is needed to perform this particular actions...");

        }
    }

    @DeleteMapping("{id}/delete-post")
    public Response<String> deletePost(@AuthenticationPrincipal UserDetailsImp authentication, @PathVariable int id){
        try{

            String curerntUser = authentication.getUsername();
            User authUser = userService.findUserByUsername(curerntUser);

             Boolean isPostDeleted = postService.deletePost(id,authUser.getId());
             if (isPostDeleted)
                 return  Response.<String>successDelete().setError("You have successfully deleted the post..... ");

             else

            return  Response.<String>badRequest().setError("The provided Id doesn't exist.. Try again.... ");

        }catch (Exception ex){

            System.out.println("Authentication Error : "+ex.getMessage());
            return  Response.<String>exception().setError("Authentiation is needed to perform this actions. ");
        }
    }


    @DeleteMapping("{comment_id}/delete-comment")
    public  Response<PostDto> deleteComment(@AuthenticationPrincipal UserDetailsImp authentication, @PathVariable int comment_id , @RequestParam(defaultValue = "0") int parent_id){

        try{
            String currentUser= authentication.getUsername();
            try{
                User authUser = userService.findUserByUsername(currentUser);
                Integer findPostIDByCommentId = postService.findPostIdByCommentId(comment_id);
                System.out.println("Here is the value of the POst ID : "+findPostIDByCommentId);



                if (findPostIDByCommentId==null)
                    return Response.<PostDto>badRequest().setError("Provided ID of the comment doesn't exist in the database ....");

              PostDto findPostByID=  postService.findPostByID(findPostIDByCommentId);
                boolean isPostOwner = false;
                // if the user_id of the auth = user_id of the post
                if (authUser.getId()== findPostByID.getUser_id()){
                    isPostOwner=true;
                }else isPostOwner =false;
                boolean isCommentedDeleted= postService.deleteComment(comment_id,findPostIDByCommentId,authUser.getId(),parent_id,isPostOwner);
                if (isCommentedDeleted){
                    PostDto response =  postService.findPostByID(findPostIDByCommentId);
                    return  Response.<PostDto>successDelete().setPayload(response);
                }else
                    return Response.<PostDto>notFound().setError("The provided commend ID or reply ID doesn't exist for this user account.....");
            }catch (Exception sqlException ){
                System.out.println("SQL Exception when deleting comment: "+sqlException.getMessage());
                return Response.<PostDto>exception().setError("SQL Exception happens.....");
            }


        }catch (Exception exception){
            System.out.println("Authentication exception occurs : "+ exception.getMessage());
            return Response.<PostDto>exception().setError("Authentication is needed to perform this action....");
        }
    }


    // Editing comments and reply
    @PatchMapping("{comment_id}/update-comment")
    public Response<PostDto> updateCommentAndReply(@AuthenticationPrincipal UserDetailsImp authentication,String content, @PathVariable int comment_id, @RequestParam(defaultValue = "0") int parent_id){
        try{
            String currentUser = authentication.getUsername();
            User authUser = userService.findUserByUsername(currentUser);

            Boolean isCommentReplyUpdate =false;
            int postId=0;
            try{
                 postId = postService.findPostIdByCommentId(comment_id);
                 isCommentReplyUpdate= postService.updateCommentReply(parent_id,comment_id,content,authUser.getId());

            }catch (Exception sqlException){
              /*  System.out.println(" The provided ID doesn't exist.. Try different one.....");*/
                System.out.println("Exception in updating reply: "+sqlException.getMessage());
                return Response.<PostDto>exception().setError("The provided ID doesn't exist.. Try different one.....");
            }

         if (isCommentReplyUpdate){
             PostDto findPostById = postService.findPostByID(postId);
             return Response.<PostDto>ok().setPayload(findPostById).setError("Successfully updated ......");
         }else
             return Response.<PostDto>exception().setError("Failed to update the content.. You are unauthorized to update this actions.");


        }catch (Exception ex){
            System.out.println("Authentication Exception: "+ex.getMessage());
            return Response.<PostDto>exception().setError("Authentication is needed to perform this particular action.. Try logging in...");

        }

    }
}
