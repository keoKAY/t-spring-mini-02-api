package com.kshrd.basicspringminiproject2.controller.restcontroller;


import com.kshrd.basicspringminiproject2.payload.response.Response;
import com.kshrd.basicspringminiproject2.service.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api/v1/file")
public class FileStorageController {

    FileStorageService fileStorageService;

    @Autowired
    FileStorageController(FileStorageService fileStorageService){
        this.fileStorageService = fileStorageService ;
    }



    @PostMapping("/file-upload")
    public Response<String> fileUpload(@RequestParam("file") MultipartFile file){

        try{

            String imageURL ="http://localhost:8080/files/"+ fileStorageService.saveFile(file);
            return Response.<String>ok().setPayload(imageURL).setError("Successfully updated....");

        }catch (Exception ex){
            System.out.println("File Exception : "+ex.getMessage());
            return  Response.<String>exception().setError("Failed to update the image..Exception Occurs.");
        }
    }

}
