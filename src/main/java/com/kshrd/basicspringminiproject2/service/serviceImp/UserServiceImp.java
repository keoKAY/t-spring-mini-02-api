package com.kshrd.basicspringminiproject2.service.serviceImp;


import com.kshrd.basicspringminiproject2.model.Role;
import com.kshrd.basicspringminiproject2.model.User;
import com.kshrd.basicspringminiproject2.payload.dao.UserSignUpDao;
import com.kshrd.basicspringminiproject2.payload.userdto.UserLoginDto;
import com.kshrd.basicspringminiproject2.payload.userequest.AccountUpdateRequest;
import com.kshrd.basicspringminiproject2.payload.userequest.UserSignUpRequest;
import com.kshrd.basicspringminiproject2.repository.UserRepository;
import com.kshrd.basicspringminiproject2.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImp implements UserService {

 @Autowired
 UserRepository userRepository;
// UserServiceImp(UserRepository userRepository){
//     this.userRepository = userRepository;
// }

    @Override
    public List<Role> roleInDatabase() {
        return userRepository.roleInDatabase();
    }

    @Override
    public User findUserByUsername(String username) {
        return userRepository.findUserByUsername(username);
    }

    @Override
    public Boolean signUpUser(UserSignUpRequest user) {
        return userRepository.signUpUser(user);
    }

    @Override
    public Boolean assignRoleToUser( int role_id, int user_id) {
        return userRepository.assignRoleToUser(role_id,user_id);
    }
    @Override
    public Boolean removeUser(int id) {
        return userRepository.removeUser(id);
    }

    @Override
    public Boolean updateUser(AccountUpdateRequest updateRequest, int id) {
        return userRepository.updateUser(updateRequest,id);
    }

    @Override
    public UserLoginDto findUserByUsernameLogin(String username) {
        return userRepository.findUserByUsernameLogin(username);
    }
}
