package com.kshrd.basicspringminiproject2.service.serviceImp;

import com.kshrd.basicspringminiproject2.service.FileStorageService;
import org.apache.tomcat.jni.File;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.*;
import java.util.UUID;


@Service
public class FileStorageServiceImp implements FileStorageService {

    Path filePathLocation ;

    @Autowired
   public FileStorageServiceImp(){
        filePathLocation= Paths.get("src/main/resources/images") ;
    }

    @Override
    public String saveFile(MultipartFile file) throws IOException {
        String filename = file.getOriginalFilename();

        //  just a simple condition to validate and simplify the name of the file that they just input
        if (filename.contains("..")){
            System.out.println("There is error of the file name ");
            return null;
        }

        String[] filePaths = filename.split("\\.");
        String extension =filePaths[1];

        filename = UUID.randomUUID() +"."+ extension;

        Path fileLocationToStore=filePathLocation.resolve(filename);
        Files.copy(file.getInputStream(),fileLocationToStore, StandardCopyOption.REPLACE_EXISTING);


        return filename;
    }
}
