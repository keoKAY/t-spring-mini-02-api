package com.kshrd.basicspringminiproject2.service.serviceImp;

import com.kshrd.basicspringminiproject2.model.Comment;
import com.kshrd.basicspringminiproject2.model.EReactionTypes;
import com.kshrd.basicspringminiproject2.model.Post;
import com.kshrd.basicspringminiproject2.payload.userdto.PostDto;
import com.kshrd.basicspringminiproject2.repository.PostRepository;
import com.kshrd.basicspringminiproject2.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImp implements PostService {
    @Autowired
    PostRepository postRepository;

    @Override
    public List<PostDto> findAllPosts(int limit , int offset,int user_id, String caption) {
        return postRepository.selectAllPosts(limit,offset,user_id,caption);
    }

    @Override
    public Integer createPost(Post post) {
        return postRepository.createPost(post);
    }

    @Override
    public Integer countTotalPost() {
        return postRepository.countTotalPost();
    }

    @Override
    public PostDto findPostByID(int id) {
        return postRepository.findPostByID(id);
    }

    @Override
    public Boolean updatePostOwnership(Boolean status, int user_id) {
        return postRepository.updatePostOwnership(status,user_id);
    }

    @Override
    public Boolean updatePost(EReactionTypes types, PostDto post, int id,int user_id,int numberOfLikes) {
        return postRepository.updatePost(types,post,id,user_id,numberOfLikes);
    }

    @Override
    public Integer getPostLikes(int id) {
        return postRepository.getPostLikes(id);
    }
    @Override
    public Boolean insertUserLikes(int user_id, int post_id) {
        return postRepository.insertUserLikes(user_id,post_id);
    }

    @Override
    public String userIdOfPost(int post_id) {
        return postRepository.userIdOfPost(post_id);
    }

    @Override
    public Boolean removeUserLikes(int post_id) {
        return postRepository.removeUserLikes(post_id);
    }

    @Override
    public Boolean addComment(int post_id, String content,int user_id) {
        return postRepository.addComment(post_id,content,user_id);
    }

    @Override
    public Boolean reply( int parent_id, int post_id, String content, int user_id) {
        return postRepository.reply( parent_id,post_id, content,  user_id);
    }

    @Override
    public Integer findPostIdByCommentId(int comment_id) {
        return postRepository.findPostIdByCommentId(comment_id);
    }

    @Override
    public Boolean deletePost(int id, int user_id) {
        return postRepository.deletePost(id,user_id);
    }

    @Override
    public Boolean deleteComment(int comment_id, int post_id, int user_id,int parent_id,boolean isOwner) {
        return postRepository.deleteComment(comment_id,post_id,user_id,  parent_id,isOwner);
    }

    @Override
    public Comment findCommentByID(int id) {
        return postRepository.findCommentByID(id);
    }

    @Override
    public Boolean updateCommentReply(int parent_id, int comment_id, String content, int user_id) {
        return postRepository.updateCommentReply(parent_id,comment_id,content,user_id);
    }

}
