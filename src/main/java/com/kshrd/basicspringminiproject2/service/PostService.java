package com.kshrd.basicspringminiproject2.service;

import com.kshrd.basicspringminiproject2.model.Comment;
import com.kshrd.basicspringminiproject2.model.EReactionTypes;
import com.kshrd.basicspringminiproject2.model.Post;
import com.kshrd.basicspringminiproject2.payload.userdto.PostDto;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface PostService {

    List<PostDto> findAllPosts(int limit, int offset,int user_id, String caption);
    Integer createPost( Post post);
    Integer countTotalPost();
    PostDto findPostByID(int id);
    Boolean updatePostOwnership(Boolean status,int user_id);
    Boolean updatePost(EReactionTypes types, PostDto post, int id , int user_id , int numberOfLikes);

    Integer getPostLikes (int id);
    Boolean insertUserLikes(int user_id,int post_id);
    String userIdOfPost( int post_id);
    Boolean removeUserLikes(int post_id);

//for the purpose of doing the comment and replying
    Boolean addComment(int post_id, String content,int user_id);
    Boolean reply( int parent_id,int post_id, String content,int user_id);
    Integer findPostIdByCommentId(int comment_id);
    Boolean deletePost(int id, int user_id);

    // comment
    Boolean deleteComment(int comment_id,int post_id, int user_id , int parent_id, boolean isPostOwner);
    Comment findCommentByID(int id);

    Boolean updateCommentReply(int parent_id,int comment_id,String content,int user_id);


}
