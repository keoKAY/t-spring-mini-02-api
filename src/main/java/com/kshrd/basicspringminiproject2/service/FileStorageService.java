package com.kshrd.basicspringminiproject2.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


@Service
public interface FileStorageService {

 public    String  saveFile(MultipartFile file) throws IOException;
}
