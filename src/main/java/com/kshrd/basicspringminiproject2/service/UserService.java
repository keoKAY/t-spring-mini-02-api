package com.kshrd.basicspringminiproject2.service;


import com.kshrd.basicspringminiproject2.model.Role;
import com.kshrd.basicspringminiproject2.model.User;
import com.kshrd.basicspringminiproject2.payload.dao.UserSignUpDao;
import com.kshrd.basicspringminiproject2.payload.userdto.UserLoginDto;
import com.kshrd.basicspringminiproject2.payload.userequest.AccountUpdateRequest;
import com.kshrd.basicspringminiproject2.payload.userequest.UserSignUpRequest;
import com.kshrd.basicspringminiproject2.repository.provider.UserProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;

import java.util.List;


public interface UserService {


    List<Role> roleInDatabase();
    User findUserByUsername(String username);
    Boolean signUpUser(UserSignUpRequest user);

  //  @InsertProvider(type = UserProvider.class , method = "assignRoleToUser")
    Boolean assignRoleToUser(int role_id,int user_id);
    Boolean removeUser( int id);
    Boolean updateUser(AccountUpdateRequest updateRequest, int id );
    UserLoginDto findUserByUsernameLogin(String username);


}
